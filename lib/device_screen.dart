
import 'dart:convert';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_blue/flutter_blue.dart';
import 'package:flutter_bluetooth_demo/widgets/ServiceTile.dart';
import 'package:flutter_bluetooth_demo/widgets/charaterisics_tile.dart';
import 'package:flutter_bluetooth_demo/widgets/descriptor_tile.dart';

class DeviceScreen extends StatefulWidget {
  final BluetoothDevice? device;
  DeviceScreen({Key? key, this.device}) : super(key: key);

  @override
  _DeviceScreenState createState() => _DeviceScreenState();
}

class _DeviceScreenState extends State<DeviceScreen> {

  final Map<Guid, List<int>> readValues = new Map<Guid, List<int>>();
  final Map<Guid, String> charValues = new Map<Guid, String>();

  List<BluetoothService> bluetoothServices = [];
  final textController = TextEditingController();

  @override
  void initState() {
    super.initState();
     //widget.device!.discoverServices().then((value) => bluetoothServices = value);

  }
  List<int> _getRandomBytes() {
    final math = Random();
    return [
      math.nextInt(255),
      math.nextInt(255),
      math.nextInt(255),
      math.nextInt(255)

    ];
  }


/*
  ListView _buildConnectDeviceView(List<BluetoothService> services) {
    List<Container> containers = <Container>[];

    for (BluetoothService? service in services) {
      List<Widget> characteristicsWidget = <Widget>[];

      for (BluetoothCharacteristic characteristic in service!.characteristics) {
        characteristicsWidget.add(
          Align(
            alignment: Alignment.centerLeft,
            child: Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Text(characteristic.uuid.toString(),
                        style: TextStyle(fontWeight: FontWeight.bold)),
                  ],
                ),
                Row(
                  children: <Widget>[
                    ..._buildReadWriteNotifyButton(characteristic),
                  ],
                ),
                Row(
                  children: <Widget>[
                    Flexible(
                      child: Text(
                          'Value: ' + charValues[characteristic.uuid].toString()),
                    ),
                  ],
                ),
                Divider(),
              ],
            ),
          ),
        );
      }
      containers.add(
        Container(
          child: ExpansionTile(
              title: Text(service.uuid.toString()),
              children: characteristicsWidget),
        ),
      );
    }

    return ListView(
      padding: const EdgeInsets.all(8),
      shrinkWrap: true,
      physics: ClampingScrollPhysics(),
      children: <Widget>[
        ...containers,
      ],
    );
  }
*/


  List<Widget> _buildServiceTiles(List<BluetoothService> services) {
    List<BluetoothService> ffb0Service = [];
    services.forEach((element) {
     if( element.uuid.toString().toUpperCase().substring(4, 8) == "FFB0"){
       ffb0Service.add(element);
     }
    });
    return ffb0Service
        .map(
          (s) => ServiceTile(
        service: s,
        characteristicTiles: s.characteristics
            .map(
              (c) => CharacteristicTile(
            characteristic: c,
            onReadPressed: () => c.read(),
            onWritePressed: () async {
              await c.write(_getRandomBytes(), withoutResponse: true);
              await c.read();
            },
            onNotificationPressed: () async {
              await c.setNotifyValue(!c.isNotifying);
              await c.read();
            },
            descriptorTiles: c.descriptors
                .map(
                  (d) => DescriptorTile(
                descriptor: d,
                onReadPressed: () => d.read(),
                onWritePressed: () => d.write(_getRandomBytes()),
              ),
            )
                .toList(),
          ),
        )
            .toList(),
      ),
    )
        .toList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.device!.name),
        actions: <Widget>[
          StreamBuilder<BluetoothDeviceState>(
            stream: widget.device!.state,
            initialData: BluetoothDeviceState.connecting,
            builder: (c, snapshot) {
              VoidCallback? onPressed;
              String? text;
              switch (snapshot.data) {
                case BluetoothDeviceState.connected:
                  onPressed = () => widget.device!.disconnect();
                  text = 'DISCONNECT';
                  break;
                case BluetoothDeviceState.disconnected:
                  onPressed = () => widget.device!.connect();
                  text = 'CONNECT';
                  break;

              }
              return FlatButton(
                  onPressed: onPressed,
                  child: Text(
                    text != null ?text :"",
                    style: Theme.of(context)
                        .primaryTextTheme
                        .button!
                        .copyWith(color: Colors.white),
                  ));
            },
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            StreamBuilder<BluetoothDeviceState>(
              stream: widget.device!.state,
              initialData: BluetoothDeviceState.connecting,
              builder: (c, snapshot) => ListTile(
                leading: (snapshot.data == BluetoothDeviceState.connected)
                    ? Icon(Icons.bluetooth_connected)
                    : Icon(Icons.bluetooth_disabled),
                title: Text(
                    'Device is ${snapshot.data.toString().split('.')[1]}.'),
                subtitle: Text('${widget.device!.id}'),
                trailing: StreamBuilder<bool>(
                  stream: widget.device!.isDiscoveringServices,
                  initialData: false,
                  builder: (c, snapshot) => IndexedStack(
                    index: snapshot.data! ? 1 : 0,
                    children: <Widget>[
                      StreamBuilder<BluetoothDeviceState>(
                          stream: widget.device!.state,
                          initialData: BluetoothDeviceState.connecting,
                          builder:(c, snapshot) {
                            return snapshot.data == BluetoothDeviceState.connected ? ElevatedButton(
                              child: Text('get services'),
                              style: ElevatedButton.styleFrom(
                                  primary: Theme.of(context).primaryColor
                              ) ,
                              //icon: Icon(Icons.refresh),
                              onPressed: () => widget.device!.discoverServices(),
                            ):Text(''); }),
                      IconButton(
                        icon: SizedBox(
                          child: CircularProgressIndicator(
                            valueColor: AlwaysStoppedAnimation(Colors.grey),
                          ),
                          width: 18.0,
                          height: 18.0,
                        ),
                        onPressed: null,
                      )
                    ],
                  ),
                ),
              ),
            ),

        /* StreamBuilder<List<BluetoothService>>(
              stream: widget.device!.services,
              initialData: [],
              builder: (c, snapshot) {
                return _buildConnectDeviceView(snapshot.data!);
              },
            )*/
            StreamBuilder<List<BluetoothService>>(
              stream: widget.device!.services,
              initialData: [],
              builder: (c, snapshot) {
                //return _buildConnectDeviceView(snapshot.data!);
                return Column(
                  children:
                  _buildServiceTiles(snapshot.data!),

                );
              },
            ),
          ],
        ),
      ),
    );
  }
  List<ButtonTheme> _buildReadWriteNotifyButton(
      BluetoothCharacteristic characteristic) {
    List<ButtonTheme> buttons = <ButtonTheme>[];

    if (characteristic.properties.read) {
      buttons.add(
        ButtonTheme(
          minWidth: 10,
          height: 20,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 4),
            child: RaisedButton(
              color: Colors.blue,
              child: Text('READ', style: TextStyle(color: Colors.white)),
              onPressed: () async {
                var sub = characteristic.value.listen((value) {
                  setState(() {
                    charValues[characteristic.uuid] = utf8.decode(value);
                  });
                });
                await characteristic.read();
                sub.cancel();
              },
            ),
          ),
        ),
      );
    }
    if (characteristic.properties.write) {
      buttons.add(
        ButtonTheme(
          minWidth: 10,
          height: 20,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 4),
            child: RaisedButton(
              child: Text('WRITE', style: TextStyle(color: Colors.white)),
              onPressed: () async {
                await showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return AlertDialog(
                        title: Text("Write"),
                        content: Row(
                          children: <Widget>[
                            Expanded(
                              child: TextField(
                                controller: textController,
                              ),
                            ),
                          ],
                        ),
                        actions: <Widget>[
                          FlatButton(
                            child: Text("Send"),
                            onPressed: () {
                              characteristic.write(
                                  utf8.encode(textController.value.text));
                              Navigator.pop(context);
                            },
                          ),
                          FlatButton(
                            child: Text("Cancel"),
                            onPressed: () {
                              Navigator.pop(context);
                            },
                          ),
                        ],
                      );
                    });
              },
            ),
          ),
        ),
      );
    }
    if (characteristic.properties.notify) {
      buttons.add(
        ButtonTheme(
          minWidth: 10,
          height: 20,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 4),
            child: RaisedButton(
              child: Text('NOTIFY', style: TextStyle(color: Colors.white)),
              onPressed: () async {
                characteristic.value.listen((value) {
                  //readValues[characteristic.uuid] = value;
                  charValues[characteristic.uuid] = utf8.decode(value);
                });
                await characteristic.setNotifyValue(true);
              },
            ),
          ),
        ),
      );
    }

    return buttons;
  }

}



