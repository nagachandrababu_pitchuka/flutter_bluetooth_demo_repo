
import 'dart:async';
import 'dart:math';
import 'package:convert/convert.dart';
import 'package:flutter/material.dart';
import 'package:flutter_blue/flutter_blue.dart';



class WeightDataPage extends StatefulWidget {
  const WeightDataPage({Key? key, required this.device}) : super(key: key);
  final BluetoothDevice device;

  @override
  _WeightDataPageState createState() => _WeightDataPageState();
}

class _WeightDataPageState extends State<WeightDataPage> {
  final String SERVICE_UUID = "FFB0";
  final String CHARACTERISTIC_READ_UUID = "FFB2";
  final String CHARACTERISTIC_WRITE_UUID = "FFB1";

  late bool isReady;
  late Stream<List<int>> stream ;


  @override
  void initState() {
    super.initState();
    isReady = false;
    connectToDevice();
  }

  connectToDevice() async {
    if (widget.device == null) {
      _pop();
      return;
    }

     Timer(const Duration(seconds: 15), () {
      if (!isReady) {
        disconnectFromDevice();
        _pop();
      }
    });

    await widget.device.connect();
    discoverServices();
  }

  disconnectFromDevice() {
    if (widget.device == null) {
      _pop();
      return;
    }

    widget.device.disconnect();
  }

  discoverServices() async {
    if (widget.device == null) {
      _pop();
      return;
    }


    List<BluetoothService> services = await widget.device.discoverServices();
    services.forEach((service) {
      if (service.uuid.toString().toUpperCase().substring(4, 8) == SERVICE_UUID) {
        service.characteristics.forEach((characteristic) async {
          if (characteristic.uuid.toString().toUpperCase().substring(4, 8) == CHARACTERISTIC_WRITE_UUID) {
            await characteristic.write(_getRandomBytes(), withoutResponse: true);
            await characteristic.read();
           await characteristic.setNotifyValue(!characteristic.isNotifying);

            await characteristic.read();

          }
          if (characteristic.uuid.toString().toUpperCase().substring(4, 8) == CHARACTERISTIC_READ_UUID) {
            await characteristic.setNotifyValue(!characteristic.isNotifying);
            //await characteristic.read();
            stream = characteristic.value;
            setState(() {
              isReady = true;
            });
          }


        });
      }


    });

    if (!isReady) {
      _pop();
    }
  }

  List<int> _getRandomBytes() {
    final math = Random();
    return [
      math.nextInt(255),
      math.nextInt(255),
      math.nextInt(255),
      math.nextInt(255)

    ];
  }

  _pop() {
    Navigator.of(context).pop(true);
  }


  String getWeightValue(List<int> value){
    if(value.length >0) {
      String lb_oz_weight ="" ;
      String hexValue = hex.encode(value).toString().toUpperCase();
      String hexWeight = hexValue.substring(4, 10);
      String hexUnit = hexValue.substring(10, 12);
      print("Weight in HEX : $hexWeight");
      print("Unit in HEX : $hexUnit");
      String weightDecimal = int.parse(hexWeight,radix: 16).toString();
      print("Weight  in Decimal : $weightDecimal");
      String binaryUnit = int.parse(hexUnit,radix: 16).toRadixString(2).padLeft(8,'0');

      print("Binary value : $binaryUnit");
      String  unitBinary = binaryUnit.substring(0,4);
      String decimalPlaces = binaryUnit.substring(4,7);
      String signDigit = binaryUnit.substring(binaryUnit.length - 1);
      print("unit : $unitBinary  decimalPlaces : $decimalPlaces signDigit : $signDigit ");
      String units =getUnit(unitBinary);
      int decimals =getDecimalPlaces(decimalPlaces);
      double weightDouble = double.parse(weightDecimal);
      double finalWeight = getRealValues(weightDouble, decimals);
      if (signDigit == "1"){
        finalWeight *= -1;
      }
      print(" Units :  $units");
      if(units == "lb:oz"){
        int lb = convertOZtoLB(finalWeight);
        double oz = convertLbToOz(finalWeight).roundToDouble();
        lb_oz_weight = lb.toString()+' : ' + oz.toString() + "  " + units;

      }

      return units == "lb:oz"?lb_oz_weight:finalWeight.toString()+ "  " + units;
    }else{
      return value.toString();
    }
  }


  String getUnit(String unitBinary) {
    switch (unitBinary) {
      case "0000" :
        return "g" ;
      case "0001" :
        return "ml" ;
      case "0010" :
        return "lb:oz" ;
      case "0011" :
        return "oz" ;
      case "0100" :
        return "kg" ;
      case "1010" :
        return "lb" ;
    // etc.
      default :
        return "" ;
    }
  }

  double getRealValues(double weightDecimal, int decimals) {
    return weightDecimal/pow(10,decimals);
  }

  int convertOZtoLB(double finalWeight) {
    double lb = finalWeight /16;
    int lbs = lb.floor();
    //double lbDecimal  = lb - lbs;
    //double oz = lbDecimal * 16;
    return lbs;
  }
  double convertLbToOz(double finalWeight) {
    double lb = finalWeight /16;
    int lbs = lb.floor();
    double lbDecimal  = lb - lbs;
    double oz = lbDecimal * 16;
    return oz;
  }

  int getDecimalPlaces(String decimalPlaces) {
    switch (decimalPlaces) {
      case "000" :
        return 0 ;
      case "001" :
        return 1 ;
      case "010" :
        return 2 ;
      case "011" :
        return 3 ;
      case "100" :
        return 4 ;
      case "101" :
        return 5 ;
      case "110" :
        return 6 ;
      case "111" :
        return 7 ;
    // etc.
      default :
        return 0 ;
    }
  }

  List<String> splitStringByLength(String str, int length) =>
      [str.substring(0, length), str.substring(length)];



  @override
  Widget build(BuildContext context) {


    return Scaffold(
      appBar: AppBar(
        title: Text(widget.device.name),
      ),
      body: Container(
          child: !isReady
              ? Center(
            child: Text(
              "Waiting...",
              style: TextStyle(fontSize: 24, color: Colors.red),
            ),
          )
              : Container(
            child: StreamBuilder<List<int>>(
              stream: stream,
              builder: (BuildContext context,
                  AsyncSnapshot<List<int>> snapshot) {
                if (snapshot.hasError)
                  return Text('Error: ${snapshot.error}');

                if (snapshot.connectionState ==
                    ConnectionState.active) {

                  var currentValue = getWeightValue(snapshot.data!);
                  return Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Expanded(
                            flex: 1,
                            child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text('WEIGHT  : ',
                                      style: TextStyle(fontSize: 14)),
                                  Text('$currentValue',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 24))
                                ]),
                          ),

                        ],
                      ));
                } else {
                  return Text('Check the stream');
                }
              },
            ),
          )),
    );
  }
}