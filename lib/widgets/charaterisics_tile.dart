import 'package:flutter/material.dart';
import 'package:flutter_blue/flutter_blue.dart';
import 'package:flutter_bluetooth_demo/widgets/descriptor_tile.dart';
import 'package:convert/convert.dart';
import 'dart:math';


class CharacteristicTile extends StatelessWidget {
  final BluetoothCharacteristic characteristic;
  final List<DescriptorTile> descriptorTiles;
  final VoidCallback onReadPressed;
  final VoidCallback onWritePressed;
  final VoidCallback onNotificationPressed;

  const CharacteristicTile(
      { Key? key,
        required this.characteristic,
        required this.descriptorTiles,
        required this.onReadPressed,
        required this.onWritePressed,
        required this.onNotificationPressed})
      : super(key: key);

  String getWeightValue(List<int> value){
    if(value.length >0) {
      String lb_oz_weight ="" ;
      String hexValue = hex.encode(value).toString().toUpperCase();
      String hexWeight = hexValue.substring(4, 10);
      String hexUnit = hexValue.substring(10, 12);
      print("Weight in HEX : $hexWeight");
      print("Unit in HEX : $hexUnit");
     String weightDecimal = int.parse(hexWeight,radix: 16).toString();
      print("Weight  in Decimal : $weightDecimal");
      String binaryUnit = int.parse(hexUnit,radix: 16).toRadixString(2).padLeft(8,'0');

      print("Binary value : $binaryUnit");
      String  unitBinary = binaryUnit.substring(0,4);
      String decimalPlaces = binaryUnit.substring(4,7);
      String signDigit = binaryUnit.substring(binaryUnit.length - 1);
      print("unit : $unitBinary  decimalPlaces : $decimalPlaces signDigit : $signDigit ");
      String units =getUnit(unitBinary);
      int decimals =getDecimalPlaces(decimalPlaces);
      double weightDouble = double.parse(weightDecimal);
      double finalWeight = getRealValues(weightDouble, decimals);
      if (signDigit == "1"){
        finalWeight *= -1;
      }
      print(" Units :  $units");
      if(units == "lb:oz"){
        int lb = convertOZtoLB(finalWeight);
        double oz = convertLbToOz(finalWeight).roundToDouble();
        lb_oz_weight = lb.toString()+' : ' + oz.toString() + "  " + units;

      }

      return units == "lb:oz"?lb_oz_weight:finalWeight.toString()+ "  " + units;
    }else{
      return value.toString();
    }
  }


  String getUnit(String unitBinary) {
    switch (unitBinary) {
      case "0000" :
        return "g" ;
      case "0001" :
        return "ml" ;
      case "0010" :
        return "lb:oz" ;
      case "0011" :
        return "oz" ;
      case "0100" :
        return "kg" ;
      case "1010" :
        return "lb" ;
    // etc.
      default :
        return "" ;
    }
  }

  double getRealValues(double weightDecimal, int decimals) {
   return weightDecimal/pow(10,decimals);
  }

  int convertOZtoLB(double finalWeight) {
    double lb = finalWeight /16;
    int lbs = lb.floor();
    //double lbDecimal  = lb - lbs;
    //double oz = lbDecimal * 16;
    return lbs;
  }
  double convertLbToOz(double finalWeight) {
    double lb = finalWeight /16;
    int lbs = lb.floor();
    double lbDecimal  = lb - lbs;
    double oz = lbDecimal * 16;
    return oz;
  }

  int getDecimalPlaces(String decimalPlaces) {
    switch (decimalPlaces) {
      case "000" :
        return 0 ;
      case "001" :
        return 1 ;
      case "010" :
        return 2 ;
      case "011" :
        return 3 ;
      case "100" :
        return 4 ;
      case "101" :
        return 5 ;
      case "110" :
        return 6 ;
      case "111" :
        return 7 ;
    // etc.
      default :
        return 0 ;
    }
  }

  List<String> splitStringByLength(String str, int length) =>
      [str.substring(0, length), str.substring(length)];
  

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<List<int>>(
      stream: characteristic.value,
      initialData: characteristic.lastValue,
      builder: (c, snapshot) {
        final value = snapshot.data;
        return ExpansionTile(
          title: ListTile(
            title: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text('Characteristic'),
                Text(
                    '0x${characteristic.uuid.toString().toUpperCase().substring(4, 8)}',
                    style: Theme.of(context).textTheme.body1!.copyWith(
                        color: Theme.of(context).textTheme.caption!.color)),
                SizedBox(height: 5,),
                Row(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    ButtonTheme(
                      minWidth: 10,
                      height: 20,
                      child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 4),
                          child: RaisedButton(
                            color: Colors.blue,
                            child: Text(
                                'READ', style: TextStyle(color: Colors.white)),
                            onPressed: onReadPressed,
                          )
                      ),
                    ),
                    ButtonTheme(
                      minWidth: 10,
                      height: 20,
                      child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 4),
                          child: RaisedButton(
                            color: Colors.blue,
                            child: Text(
                                'WRITE', style: TextStyle(color: Colors.white)),
                            onPressed: onWritePressed,
                          )
                      ),
                    ),
                    IconButton(
                      icon: Icon(
                          characteristic.isNotifying
                              ? Icons.sync_disabled
                              : Icons.sync,
                          color: Theme.of(context).iconTheme.color!.withOpacity(0.5)),
                      onPressed: onNotificationPressed,
                    )
                  ],
                ),

              ],
            ),
            subtitle: Text(getWeightValue(value!).toString()),
            contentPadding: EdgeInsets.all(0.0),
          ),


          children: descriptorTiles,
        );
      },
    );
  }
}
